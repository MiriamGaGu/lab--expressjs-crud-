require('dotenv').config();
const express = require('express');
const chalk = require('chalk');
const morgan = require('morgan');
const app = express();
const bodyParser = require('body-parser');

const PORT = process.env.PORT || 3001;

const api = require('./src/routes/api')

const MON_API = process.env.MONG_API;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())

//middleware
app.use(morgan('combined'));

app.use('/api', api)

// app.use('/', (req, res) => {
//     res.send('Hello Friend')
// });

//error 404
app.use((err, req, res, next) => {
    const ERROR = {
        message: '500. Server Error'
    }
    res
        .status(500)
        .json(ERROR);
});

app.listen(PORT, () => {
    const msg = chalk.blue(`Node Server is running on PORT ${PORT}`);

    console.log(msg);

})